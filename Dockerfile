FROM python:3  
COPY firewallv2.py . 
EXPOSE 9080 
RUN pip install flask
RUN pip install requests  
CMD [ "python", "./firewallv2.py" ]

