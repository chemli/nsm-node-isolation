url = "http://192.168.49.2:31825/"
from flask import Flask, request, render_template
import requests

 
app = Flask(__name__)

@app.route('/', methods=["GET"])
def get():
    return  render_template("main.html")

@app.route('/<appName>', methods=["GET"])
def getapp(appName):
    return requests.post(url,json = {"params":"", "appName":str(appName)}).text
    
    
if __name__ == '__main__':
    app.run(debug=True, port=8080)
