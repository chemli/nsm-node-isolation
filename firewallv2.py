from flask import Flask, request
import requests
DNS = {"app1":"10.96.118.2" ,"app2":"10.104.169.18" , "app3":"10.108.236.164" } 
authorizedApps = ["app1","app2"]
app = Flask(__name__)

@app.route('/', methods=["POST"])
def home():
    input_json = request.get_json()
    appName = input_json["appName"]
    if not appName in authorizedApps:
        return "<h1>Hello from firewall</h1> <h2> You are not allowed to reach this application</h2> <h2> Please contact the cluster admin to add it in your authorized applications list</h2>" 
    params=input_json["params"]
    try:
        req = requests.get("http://"+DNS[appName]+":9080")
    except:
        return "Firewall says : Your application is not found in the cluster"    
    res = req.text
    return "<h1>Hello from firewall</h1> <h2> This is "+appName+"'s response to your request</h2> <h2>"+ res + "</h2>" 

    
@app.route('/', methods=["GET"])
def get():
    return "Hello from the firewall!"
    
if __name__ == '__main__':
    app.run(debug=True, port=9080)
